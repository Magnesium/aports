# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kiconthemes
pkgver=5.91.0
pkgrel=0
pkgdesc="Support for icon themes"
# armhf blocked by extra-cmake-module
# s390x and riscv64 blocked by polkit
arch="all !armhf !s390x !riscv64"
url="https://community.kde.org/Frameworks"
license="LGPL-2.1-only"
depends_dev="
	karchive-dev
	kconfigwidgets-dev
	kcoreaddons-dev
	ki18n-dev
	kitemviews-dev
	kwidgetsaddons-dev
	qt5-qtsvg-dev
	"
makedepends="$depends_dev
	doxygen
	extra-cmake-modules
	qt5-qttools-dev
	"
checkdepends="xvfb-run"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kiconthemes-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	cd build
	# kiconloader_unittest is broken
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest -E "(kiconloader_unittest|kiconloader_resourcethemetest)"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
a7441a59872d766162a310c4fdeae27d416ef91977f9ded6970532766da3aaf2bfa7b5cd34f4d97cc04a27d2cf6b58b6735e5d3ba3a9171a21b5389d47df824c  kiconthemes-5.91.0.tar.xz
"

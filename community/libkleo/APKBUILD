# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=libkleo
pkgver=21.12.2
pkgrel=0
pkgdesc="KDE PIM cryptographic library"
# armhf blocked by extra-cmake-modules
# s390x and riscv64 blocked by polkit -> kpimtextedit
arch="all !armhf !s390x !riscv64"
url="https://kontact.kde.org"
license="GPL-2.0-or-later"
# TODO: Maybe replace gnupg with specific gnupg subpackages.
depends="gnupg"
makedepends="
	boost-dev
	extra-cmake-modules
	gpgme-dev
	kcodecs-dev
	kcompletion-dev
	kconfig-dev
	kcoreaddons-dev
	ki18n-dev
	kitemmodels-dev
	kpimtextedit-dev
	kwidgetsaddons-dev
	kwindowsystem-dev
	qt5-qtbase-dev
	"
checkdepends="xvfb-run"
source="https://download.kde.org/stable/release-service/$pkgver/src/libkleo-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-lang"

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}
sha512sums="
d51fa6a6ca4f5e20457fabd13d42914743efaa500b8ea48ba177c4908c255038acea950e02a4796ed4cf4a289bb671490434adfe4bef9f7c2f65c965a1ff20b4  libkleo-21.12.2.tar.xz
"

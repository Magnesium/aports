# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=print-manager
pkgver=21.12.2
pkgrel=0
# armhf blocked by qt5-qtdeclarative
# s390x and riscv64 blocked by polkit -> kio
arch="all !armhf !s390x !riscv64"
url="https://www.kde.org/applications/utilities/"
pkgdesc="A tool for managing print jobs and printers"
license="GPL-2.0-or-later"
makedepends="
	cups-dev
	extra-cmake-modules
	kcmutils-dev
	kconfig-dev
	kconfigwidgets-dev
	kcoreaddons-dev
	kdbusaddons-dev
	ki18n-dev
	kiconthemes-dev
	kio-dev
	knotifications-dev
	kwidgetsaddons-dev
	kwindowsystem-dev
	plasma-framework-dev
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/print-manager-$pkgver.tar.xz"
subpackages="$pkgname-lang"
options="!check" # No tests available

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
e18ee726bde5da3cee5ddd07a0428dd81d0133eee87a680628adb3ed53ee6a9558cacf3b7f0102cdaad68c4f4431ad4ee4af95e94a1dc2228b5c3c13dfb03940  print-manager-21.12.2.tar.xz
"

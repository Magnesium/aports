# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=cervisia
pkgver=21.12.2
pkgrel=0
# armhf blocked by extra-cmake-modules
# s390x and riscv64 blocked by polkit -> kparts
arch="all !armhf !s390x !riscv64"
url="https://kde.org/applications/development/org.kde.cervisia"
pkgdesc="A user friendly version control system front-end"
license="GPL-2.0-or-later AND GFDL-1.2-only"
makedepends="
	extra-cmake-modules
	kdesu-dev
	kdoctools-dev
	kiconthemes-dev
	kinit-dev
	kitemviews-dev
	knotifications-dev
	kparts-dev
	kwidgetsaddons-dev
	qt5-qtbase-dev
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/cervisia-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"
options="!check" # No tests

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
373bc761c2d8ebeeb75cb1d6185e60d0faa2b9b245e9de326213ae40cac756cade832d6fb7c6ba5be9d2838f6c8ace4d1b9c1070590ec0b0d2e9b7b950eac414  cervisia-21.12.2.tar.xz
"

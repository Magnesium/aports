# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=libkcddb
pkgver=21.12.2
pkgrel=0
# armhf blocked by extra-cmake-modules
# s390x and riscv64 blocked by polkit -> kio
arch="all !armhf !s390x !riscv64"
url="https://www.kde.org/applications/multimedia/"
pkgdesc="KDE CDDB library"
license="LGPL-2.0-or-later AND GPL-2.0-or-later"
makedepends="
	extra-cmake-modules
	kcodecs-dev
	kconfig-dev
	kdoctools-dev
	ki18n-dev
	kio-dev
	kwidgetsaddons-dev
	libmusicbrainz-dev
	qt5-qtbase-dev
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/libkcddb-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
options="net" # Required for tests

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_TESTING=ON
	cmake --build build
}

check() {
	cd build
	# musicbrainztest-severaldiscs, asynccddblookuptest and synccddblookuptest are broken
	# asynchttplookuptest fails to setup dbus
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest -E "(musicbrainztest-severaldiscs|asynchttplookuptest|asynccddblookuptest|synccddblookuptest)"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
fc3dc14e73a8ce0b2fab08c759f7a351f6b031081b369014c4fb925f9e4a27d37c712802b3771279056e7d09e097f8d4e57cc5c3107d0b640d99d6be0c32ca8a  libkcddb-21.12.2.tar.xz
"

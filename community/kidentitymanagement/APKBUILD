# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kidentitymanagement
pkgver=21.12.2
pkgrel=0
pkgdesc="KDE PIM libraries"
# armhf blocked by extra-cmake-modules
# s390x and riscv64 blocked by polkit -> kio
arch="all !armhf !s390x !riscv64"
url="https://kontact.kde.org"
license="LGPL-2.0-or-later"
depends_dev="
	kcodecs-dev
	kcompletion-dev
	kconfig-dev
	kcoreaddons-dev
	kiconthemes-dev
	kio-dev
	kpimtextedit-dev
	ktextwidgets-dev
	kxmlgui-dev
	qt5-qtbase-dev
	"
makedepends="$depends_dev extra-cmake-modules"
checkdepends="xvfb-run"
source="https://download.kde.org/stable/release-service/$pkgver/src/kidentitymanagement-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-lang"

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build

	# kpimidentity-signaturetest is broken
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest -E "kpimidentity-signaturetest"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}
sha512sums="
f3b87978130cb969f717b07877f0d5be7aab1d3cb898083b5487ec801d48e4b797934dd7e63f1449e1dc5cac894cbf6b50811bfd98f64425beac11f38a37f19a  kidentitymanagement-21.12.2.tar.xz
"

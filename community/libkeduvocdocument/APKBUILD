# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=libkeduvocdocument
pkgver=21.12.2
pkgrel=0
# s390x and riscv64 blocked by polkit -> kio
arch="all !armhf !s390x !riscv64"
url="https://edu.kde.org"
pkgdesc="Library to parse, convert, and manipulate KVTML files"
license="GPL-2.0-or-later AND GFDL-1.2-only"
makedepends="
	extra-cmake-modules
	karchive-dev
	ki18n-dev
	kio-dev
	qt5-qtbase-dev
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/libkeduvocdocument-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-lang"

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
b7ffe432c6792f8d5bf1a0ab9e1497af19963d9488162d8cf0d3763a1d341a265d2d9fd08d9ba735fd1af71ca371397036bfc87cdb15d49616a2400f371f2cf9  libkeduvocdocument-21.12.2.tar.xz
"

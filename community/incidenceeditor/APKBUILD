# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=incidenceeditor
pkgver=21.12.2
pkgrel=0
pkgdesc="KDE PIM incidence editor"
# armhf blocked by extra-cmake-modules
# ppc64le blocked by kmailtransport -> qt5-qtwebengine
# s390x and riscv64 blocked by polkit -> kio
arch="all !armhf !ppc64le !s390x !riscv64"
url="https://kontact.kde.org/"
license="LGPL-2.0-or-later AND GPL-2.0-or-later"
depends_dev="
	akonadi-dev
	akonadi-mime-dev
	calendarsupport-dev
	eventviews-dev
	kcalendarcore-dev
	kcalutils-dev
	kcodecs-dev
	kdiagram-dev
	ki18n-dev
	kiconthemes-dev
	kio-dev
	kldap-dev
	kmailtransport-dev
	kmime-dev
	libkdepim-dev
	qt5-qtbase-dev
	"
makedepends="$depends_dev
	extra-cmake-modules
	"
checkdepends="xvfb-run"
source="https://download.kde.org/stable/release-service/$pkgver/src/incidenceeditor-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-lang"

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build

	# akonadi-sqlite-incidencedatetimetest and akonadi-mysql-incidencedatetimetest require running DBus
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest -E "akonadi-(sqlite|mysql)-incidencedatetimetest"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}
sha512sums="
3f5947865ac40f0f769fe8790f0597fe18e0d1009f813cca5a83d1d0b75ebd2c7a95f1a73cdcde9e83398031f0f129b751152b8d5ee23beec7465992665268cb  incidenceeditor-21.12.2.tar.xz
"

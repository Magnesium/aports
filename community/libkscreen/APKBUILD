# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=libkscreen
pkgver=5.24.2
pkgrel=0
pkgdesc="KDE screen management software"
arch="all !armhf" # armhf blocked by extra-cmake-modules
url="https://kde.org/plasma-desktop/"
license="LGPL-2.1-or-later AND GPL-2.0-or-later AND (GPL-2.0-only OR GPL-3.0-only)"
depends_dev="
	kwayland-dev
	qt5-qtbase-dev
	qt5-qtx11extras-dev
	"
makedepends="$depends_dev
	doxygen
	extra-cmake-modules
	graphviz
	plasma-wayland-protocols
	qt5-qttools-dev
	"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
source="https://download.kde.org/$_rel/plasma/$pkgver/libkscreen-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc"
options="!check" # Fails due to requiring dbus-x11 and it running

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}
sha512sums="
d8adfe02a9ef3e0fc2535601610d78db75c5882409156f191801a2e9b02a36446c3c3c0dc271a9624ed59df66a0eaa0fb62e0929cbcca5767e5859179abe354f  libkscreen-5.24.2.tar.xz
"

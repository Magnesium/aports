# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kldap
pkgver=21.12.2
pkgrel=0
pkgdesc="LDAP access API for KDE"
# armhf blocked by extra-cmake-modules
# s390x and riscv64 blocked by polkit -> kio
arch="all !armhf !s390x !riscv64"
url="https://github.com/kde/kldap"
license="LGPL-2.0-or-later"
depends_dev="
	kcompletion-dev
	kdoctools-dev
	ki18n-dev
	kio-dev
	kwidgetsaddons-dev
	openldap-dev
	qt5-qtkeychain-dev
	"
makedepends="$depends_dev
	extra-cmake-modules
	cyrus-sasl-dev
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/kldap-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --build build --target install
}
sha512sums="
0d5eb3bf4bce3068c8ade6e2309bb004cefe439366713a164dffe74cc04ff392466c947b0b28abf3c9be48db16830dfb0ee497e907260abb8c38194b85bdc8e4  kldap-21.12.2.tar.xz
"

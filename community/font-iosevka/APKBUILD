# Contributor: psykose <alice@ayaya.dev>
# Maintainer: psykose <alice@ayaya.dev>

pkgname=font-iosevka
pkgver=15.0.1
pkgrel=0
pkgdesc="Versatile typeface for code, from code."
url="https://typeof.net/Iosevka/"
arch="noarch"
options="!check" # no testsuite
license="OFL-1.1"
depends="fontconfig"
subpackages="
	$pkgname-base
	$pkgname-slab
	$pkgname-curly
	$pkgname-curly-slab:curly_slab
	$pkgname-aile
	$pkgname-etoile
	"
source="
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/super-ttc-iosevka-$pkgver.zip
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/super-ttc-iosevka-slab-$pkgver.zip
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/super-ttc-iosevka-curly-$pkgver.zip
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/super-ttc-iosevka-curly-slab-$pkgver.zip
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/super-ttc-iosevka-aile-$pkgver.zip
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/super-ttc-iosevka-etoile-$pkgver.zip
	"

builddir="$srcdir"

package() {
	depends="
		$pkgname-base
		$pkgname-slab
		$pkgname-curly
		$pkgname-curly-slab
		$pkgname-aile
		$pkgname-etoile
	"

	mkdir -p "$pkgdir"/usr/share/fonts/TTC
	mv "$builddir"/*.ttc "$pkgdir"/usr/share/fonts/TTC
}

base() {
	pkgdesc="$pkgdesc (Iosevka)"
	amove usr/share/fonts/TTC/iosevka.ttc
}

slab() {
	pkgdesc="$pkgdesc (Iosevka Slab)"
	amove usr/share/fonts/TTC/iosevka-slab.ttc
}

curly() {
	pkgdesc="$pkgdesc (Iosevka Curly)"
	amove usr/share/fonts/TTC/iosevka-curly.ttc
}

curly_slab() {
	pkgdesc="$pkgdesc (Iosevka Curly Slab)"
	amove usr/share/fonts/TTC/iosevka-curly-slab.ttc
}

aile() {
	pkgdesc="$pkgdesc (Iosevka Aile)"
	amove usr/share/fonts/TTC/iosevka-aile.ttc
}

etoile() {
	pkgdesc="$pkgdesc (Iosevka Etoile)"
	amove usr/share/fonts/TTC/iosevka-etoile.ttc
}

sha512sums="
30b837e8b4400f3629d01264da93ab03400ee763b52932aa7c3bd74eeb51a43e9f430a5052da7f3d62bed3c1bb90dda5b3493dbb3cc06a26557212b8791ca56e  super-ttc-iosevka-15.0.1.zip
9da06fee742ad280191d7fe254eb12b79420410b80798ebfbc6372f4cf9e1c98fe341bb49412f0c5932cf5bfd154297aa5d5739ef965ffef1eca01a383913025  super-ttc-iosevka-slab-15.0.1.zip
4656343237631083f5b8a1310a9c4ce032b0545b02f2a61c5a02e739d1433ca9ed02da46054e2a119eeba7c5d5c7ab0ba3eaeb47437171f83cda5e4e0662fcff  super-ttc-iosevka-curly-15.0.1.zip
6408f0b1e16e7987a6a9f4cc84b53aa3dd404b704f4791885da6c54e3e9b36c0c14364551efcc837014abb47e786171a624ac09c3328f6cb6570eec37a5b643d  super-ttc-iosevka-curly-slab-15.0.1.zip
7d8be8f223f106776e0bc031df7764a25b6971ca1063b1f5be28bfd2b8dd3ff8665cd51d0546975d5e0bd46d0f3d74197dd9cf4978adbc28d0ca66c1c0834fc9  super-ttc-iosevka-aile-15.0.1.zip
070077a0887d0e7c6fe18eeaafeb028ce8817412b1529c281ac674ef7bfd24abb8b3188197068aa513fffe3717ecb1a5cea99b031cbc2a9081ecfdc31db0eca3  super-ttc-iosevka-etoile-15.0.1.zip
"

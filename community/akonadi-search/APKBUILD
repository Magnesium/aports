# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=akonadi-search
pkgver=21.12.2
pkgrel=0
pkgdesc="Libraries and daemons to implement searching in Akonadi"
# armhf blocked by extra-cmake-modules
# riscv64 blocked by akonadi
# s390x blocked by multiple KDE Frameworks
# ppc64le blocked by qt5-qtwebengine -> akonadi
arch="all !armhf !s390x !riscv64 !ppc64le"
url="https://community.kde.org/KDE_PIM"
license="( GPL-2.0-only OR GPL-3.0-only ) AND ( LGPL-2.1-only OR LGPL-3.0-only )"
depends_dev="
	akonadi-dev>=$pkgver
	akonadi-mime-dev>=$pkgver
	kcalendarcore-dev
	kcmutils-dev
	kconfig-dev
	kcontacts-dev
	kcrash-dev
	kdbusaddons-dev
	ki18n-dev
	kio-dev
	kmime-dev
	krunner-dev
	qt5-qtbase-dev
	xapian-core-dev
	"
makedepends="$depends_dev
	extra-cmake-modules
	"
checkdepends="xvfb-run"
source="https://download.kde.org/stable/release-service/$pkgver/src/akonadi-search-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-lang"

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build

	# akonadi-sqlite-schedulertest, konadi-sqlite-collectionindexingjobtest,
	# akonadi-mysql-schedulertest and akonadi-mysql-collectionindexingjobtest
	# require running dbus server

	# Prevent the lines using more than 80 characters by looping
	local skipped_tests="("
	local tests="
		akonadi-sqlite-scheduler
		akonadi-sqlite-collectionindexingjob
		akonadi-mysql-scheduler
		akonadi-mysql-collectionindexingjob"
	for test in $tests; do
		skipped_tests="$skipped_tests|$test"
	done
	skipped_tests="$skipped_tests)test"
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest -E "$skipped_tests"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}
sha512sums="
a0f57c55f6064649a780d1b5936fd64cc05641bcbef7946bf2cd0ff8febf1388cfb3913135781cf8250a9a27bf16b39c90b5ef9ee3c67310e2fd6294d141fc82  akonadi-search-21.12.2.tar.xz
"

# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kontactinterface
pkgver=21.12.2
pkgrel=0
# armhf blocked by extra-cmake-modules
# s390x and riscv64 blocked by polkit -> kparts
arch="all !armhf !s390x !riscv64"
url="https://kontact.kde.org/"
pkgdesc="Kontact Plugin Interface Library"
license="LGPL-2.0-only OR LGPL-3.0-only"
depends_dev="
	kcoreaddons-dev
	ki18n-dev
	kiconthemes-dev
	kparts-dev
	kwindowsystem-dev
	kxmlgui-dev
	"
makedepends="$depends_dev extra-cmake-modules"
source="https://download.kde.org/stable/release-service/$pkgver/src/kontactinterface-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-lang"

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
a3815078e2bc33ca693aacd3f7b6036de5cf12aaa36b2cfbac6f156d5e6fb8aa40d553b00f016c5c5407f71ebc26ebbeb2407663d97af88116d9c9b2d40aa010  kontactinterface-21.12.2.tar.xz
"

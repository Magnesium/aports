# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kxmlgui
pkgver=5.91.0
pkgrel=0
pkgdesc="User configurable main windows"
# armhf blocked by extra-cmake-modules
# s390x and riscv64 blocked by polkit
arch="all !armhf !s390x !riscv64"
url="https://community.kde.org/Frameworks"
license="LGPL-2.1-only AND LGPL-2.1-or-later"
depends_dev="
	attica-dev
	kconfig-dev
	kconfigwidgets-dev
	kcoreaddons-dev
	kglobalaccel-dev
	ki18n-dev
	kiconthemes-dev
	kitemviews-dev
	ktextwidgets-dev
	kwidgetsaddons-dev
	qt5-qtbase-dev
	"
makedepends="$depends_dev
	doxygen
	extra-cmake-modules
	qt5-qttools-dev
	"
checkdepends="
	mesa-dri-swrast
	xvfb-run
	"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kxmlgui-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	cd build
	# kxmlgui_unittest, ktoolbar_unittest and ktooltiphelper_unittest are broken
	LC_ALL=C CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest -E '(kxmlgui|ktoolbar|ktooltiphelper)_unittest'
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}
sha512sums="
dff4f4f030c863f677a654b59aa7c4ff91d7342e00525070013093240e4de3e6576a494f2e8330ea5740f9a49a6fb8692faf93554d3f1ad4805e80d5ac765624  kxmlgui-5.91.0.tar.xz
"

# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=calendarsupport
pkgver=21.12.2
pkgrel=0
pkgdesc="Library providing calendar support"
# armhf blocked by extra-cmake-modules
# ppc64le blocked by akonadi-calendar
# s390x, and riscv64 blocked by polkit -> kio
arch="all !armhf !ppc64le !s390x !riscv64"
url="https://kontact.kde.org"
license="GPL-2.0-or-later AND Qt-GPL-exception-1.0 AND LGPL-2.0-or-later"
depends_dev="
	akonadi-calendar-dev
	akonadi-dev
	akonadi-mime-dev
	akonadi-notes-dev
	kcalendarcore-dev
	kcalutils-dev
	kcodecs-dev
	kguiaddons-dev
	kholidays-dev
	ki18n-dev
	kidentitymanagement-dev
	kio-dev
	kmime-dev
	pimcommon-dev
	qt5-qtbase-dev
	"
makedepends="$depends_dev
	extra-cmake-modules
	qt5-qttools-dev
	"
checkdepends="xvfb-run"
source="https://download.kde.org/stable/release-service/$pkgver/src/calendarsupport-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-lang"

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}
sha512sums="
eb742f5e836d7c7ce9882e8c872fd4c79907cd302d4c121d6582093266ed60bc1e657d657577888e84eb944cce98e2191d8eaf285ed40c1a7e2d1eef0544c72d  calendarsupport-21.12.2.tar.xz
"

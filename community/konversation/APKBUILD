# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=konversation
pkgver=21.12.2
pkgrel=0
# armhf blocked by extra-cmake-modules
# s390x and riscv64 blocked by polkit -> kio
arch="all !armhf !s390x !riscv64"
url="https://konversation.kde.org/"
pkgdesc="A user-friendly and fully-featured IRC client"
license="GPL-2.0-or-later"
makedepends="
	extra-cmake-modules
	karchive-dev
	kbookmarks-dev
	kconfig-dev
	kconfigwidgets-dev
	kcoreaddons-dev
	kcrash-dev
	kdbusaddons-dev
	kdoctools-dev
	kglobalaccel-dev
	ki18n-dev
	kiconthemes-dev
	kidletime-dev
	kio-dev
	kitemviews-dev
	knewstuff-dev
	knotifications-dev
	knotifyconfig-dev
	kparts-dev
	kwallet-dev
	kwidgetsaddons-dev
	kwindowsystem-dev
	phonon-dev
	qca-dev
	qt5-qtbase-dev
	solid-dev
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/konversation-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"
options="!check" # No tests

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
5e438d04f89973ac99948648cec30272397c28d2e42c7cc9d8a59310b6fe0f3ed4fe64ef4b7d50b40d66f37956acc966bfff6bc39169169249ddf39aadcb276e  konversation-21.12.2.tar.xz
"

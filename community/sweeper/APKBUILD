# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=sweeper
pkgver=21.12.2
pkgrel=0
# armhf blocked by extra-cmake-modules
# s390x and riscv64 blocked by polkit -> kio
arch="all !armhf !s390x !riscv64"
url="https://kde.org/applications/utilities/org.kde.sweeper"
pkgdesc="System cleaner to help clean unwanted traces the user leaves on the system"
license="LGPL-2.1-or-later"
makedepends="
	extra-cmake-modules
	kactivities-stats-dev
	kbookmarks-dev
	kconfig-dev
	kconfigwidgets-dev
	kcoreaddons-dev
	kcrash-dev
	kdoctools-dev
	ki18n-dev
	kio-dev
	ktextwidgets-dev
	kxmlgui-dev
	qt5-qtbase-dev
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/sweeper-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
c8e14f6c61386fa2765c5c57b0332d3d133dd6af3a305f464eadc0a86800ffa72dcae8e9873635029f4e2d5b5b589d8b8a9fbd107a5993feceffdd23f1b63b92  sweeper-21.12.2.tar.xz
"

# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=plasma-workspace-wallpapers
pkgver=5.24.2
pkgrel=0
pkgdesc="Wallpapers for the Plasma Workspace"
arch="noarch !armhf" # armhf blocked by extra-cmake-modules
url="https://kde.org/plasma-desktop/"
license="GPL-2.0-or-later"
makedepends="extra-cmake-modules"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
source="https://download.kde.org/$_rel/plasma/$pkgver/plasma-workspace-wallpapers-$pkgver.tar.xz"
options="!check" # No tests available

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
ad8fff49399111824793fe5bf68f7906774b4b5255f3f0b67dad2c26076bdd4dc6273270ae4e667585adb64ad23458540c53676fbd827225e773033548600a6f  plasma-workspace-wallpapers-5.24.2.tar.xz
"

# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kbookmarks
pkgver=5.91.0
pkgrel=0
pkgdesc="Support for bookmarks and the XBEL format"
# armhf blocked by extra-cmake-modules
# s390x and riscv64 blocked by polkit
arch="all !armhf !s390x !riscv64"
url="https://community.kde.org/Frameworks"
license="LGPL-2.1-or-later"
depends_dev="
	kcodecs-dev
	kconfig-dev
	kconfigwidgets-dev
	kcoreaddons-dev
	kwidgetsaddons-dev
	kxmlgui-dev
	qt5-qtbase-dev
	"
makedepends="$depends_dev
	doxygen
	extra-cmake-modules
	qt5-qttools-dev
	"
checkdepends="xvfb-run"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kbookmarks-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}
sha512sums="
3ab1e0dffdce40998d888cc77af91d3fafa8edeb9e1a746887b4ebe4d3c43eeeb19a305f1f4ebbac925a60785686f6d60e4c3e2f92bca490d59655882148aea8  kbookmarks-5.91.0.tar.xz
"

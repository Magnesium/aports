# Contributor: solidnerd <niclas@mietz.io>
# Maintainer: solidnerd <niclas@mietz.io>
pkgname=glab
pkgver=1.22.0
pkgrel=0
pkgdesc="Open source GitLab CLI tool written in Go"
url="https://github.com/profclems/glab"
arch="all"
license="MIT"
depends="git"
makedepends="go"
options="!check chmod-clean" # Need to be run in a git repo
subpackages="
	$pkgname-bash-completion
	$pkgname-zsh-completion
	$pkgname-fish-completion
	"
source="$pkgname-$pkgver.tar.gz::https://github.com/profclems/glab/archive/v$pkgver.tar.gz"

export CGO_ENABLED=0

build() {
	make build GLAB_VERSION=$pkgver
}

package() {
	install -Dm755 "$builddir"/bin/glab -t "$pkgdir"/usr/bin/

	mkdir -p \
		"$pkgdir"/usr/share/bash-completion/completions \
		"$pkgdir"/usr/share/zsh/site-functions \
		"$pkgdir"/usr/share/fish/completions

	bin/glab completion --shell bash > "$pkgdir"/usr/share/bash-completion/completions/glab.bash
	bin/glab completion --shell zsh > "$pkgdir"/usr/share/zsh/site-functions/_glab
	bin/glab completion --shell fish > "$pkgdir"/usr/share/fish/completions/glab.fish
}
sha512sums="
1948e9c330c31240fcfa5fdcae597b4ae11b1df2d3d5d85a095913988a8f5483107202ce66a474c081cc4b8a44cc5b317dc1f65f75f6b0c3e11308e2bb2b91b5  glab-1.22.0.tar.gz
"

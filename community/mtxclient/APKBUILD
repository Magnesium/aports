# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Síle Ekaterin Liszka <sheila@vulpine.house>
pkgname=mtxclient
pkgver=0.6.2
pkgrel=0
pkgdesc="Client API library for Matrix, built on top of Boost.Asio"
url="https://github.com/nheko-reborn/mtxclient"
arch="all"
license="MIT"
depends_dev="boost-dev coeurl-dev libsodium-dev nlohmann-json olm-dev
	openssl1.1-compat-dev zlib-dev"
makedepends="$depends_dev cmake gtest-dev"
subpackages="$pkgname-dev"
source="https://github.com/nheko-reborn/mtxclient/archive/v$pkgver/mtxclient-v$pkgver.tar.gz"
options="!check" # Requires running Synapse instance

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_LIB_EXAMPLES=OFF
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
91ac33b16fb2685f1aee3e76f3fe334a34e8403d11be8a065e10dac0181333131654f26886a8cf5b25ded1960010c06a259e7d2e5f195a28f7cc8041ca9dbff9  mtxclient-v0.6.2.tar.gz
"

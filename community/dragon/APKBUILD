# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=dragon
pkgver=21.12.2
pkgrel=0
# armhf blocked by extra-cmake-modules
# s390x and riscv64 blocked by polkit -> kio
arch="all !armhf !s390x !riscv64"
url="https://kde.org/applications/multimedia/org.kde.dragonplayer"
pkgdesc="A multimedia player where the focus is on simplicity, instead of features"
license="GPL-2.0-only OR GPL-3.0-only"
depends="phonon-backend-gstreamer"
makedepends="
	extra-cmake-modules
	kconfig-dev
	kconfigwidgets-dev
	kcoreaddons-dev
	kcrash-dev
	kdbusaddons-dev
	kdoctools-dev
	ki18n-dev
	kio-dev
	kjobwidgets-dev
	knotifications-dev
	kparts-dev
	kwidgetsaddons-dev
	kwindowsystem-dev
	kxmlgui-dev
	phonon-dev
	qt5-qtbase-dev
	solid-dev
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/dragon-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"
options="!check" # No tests

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
d024a7607259743ef8851aefdbed5e95f72e87c7e3bd071f1e15207b24dfec684cc4e03c19ed210fb5b658078732c3e6c69218c68fec8b22851111fa2277595c  dragon-21.12.2.tar.xz
"
